﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SimpleChiprul.Models
{
    public partial class DB_ToDoContext : DbContext
    {
        public DB_ToDoContext()
        {
        }

        public DB_ToDoContext(DbContextOptions<DB_ToDoContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TbltTodo> TbltTodos { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TbltTodo>(entity =>
            {
                entity.HasKey(e => e.TodoId);

                entity.ToTable("tblt_todo");

                entity.Property(e => e.TodoId)
                    .HasColumnName("todoId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("createdAt")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TaskDesc)
                    .HasColumnType("text")
                    .HasColumnName("taskDesc");

                entity.Property(e => e.TaskName)
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasColumnName("taskName");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
