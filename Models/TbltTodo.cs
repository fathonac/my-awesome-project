﻿using System;
using System.Collections.Generic;

namespace SimpleChiprul.Models
{
    public partial class TbltTodo
    {
        public Guid TodoId { get; set; }
        public string TaskName { get; set; } = null!;
        public string TaskDesc { get; set; } = null!;
        public DateTime CreatedAt { get; set; }
    }
}
