﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SimpleChiprul.Models;

namespace SimpleChiprul.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodosController : ControllerBase
    {
        private readonly DB_ToDoContext _context;

        public TodosController(DB_ToDoContext context)
        {
            _context = context;
        }

        // GET: api/Todos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TbltTodo>>> GetTbltTodos()
        {
            return await _context.TbltTodos.ToListAsync();
        }

        // GET: api/Todos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TbltTodo>> GetTbltTodo(Guid id)
        {
            var tbltTodo = await _context.TbltTodos.FindAsync(id);

            if (tbltTodo == null)
            {
                return NotFound();
            }

            return tbltTodo;
        }

        // PUT: api/Todos/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTbltTodo(Guid id, TbltTodo tbltTodo)
        {
            if (id != tbltTodo.TodoId)
            {
                return BadRequest();
            }

            _context.Entry(tbltTodo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TbltTodoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Todos
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<TbltTodo>> PostTbltTodo(TbltTodo tbltTodo)
        {
            _context.TbltTodos.Add(tbltTodo);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTbltTodo", new { id = tbltTodo.TodoId }, tbltTodo);
        }

        // DELETE: api/Todos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTbltTodo(Guid id)
        {
            var tbltTodo = await _context.TbltTodos.FindAsync(id);
            if (tbltTodo == null)
            {
                return NotFound();
            }

            _context.TbltTodos.Remove(tbltTodo);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool TbltTodoExists(Guid id)
        {
            return _context.TbltTodos.Any(e => e.TodoId == id);
        }
    }
}
